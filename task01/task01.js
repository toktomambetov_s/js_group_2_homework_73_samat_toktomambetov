const express = require('express');

const app = express();
const port = 8000;

app.get('/:line', (req, res) => {
    res.send(`${req.params.line}`);
});

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});