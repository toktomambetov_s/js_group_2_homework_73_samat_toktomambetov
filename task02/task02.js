const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;
const password = 'password';

app.get('/encode/:line', (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(`${req.params.line}`));
});

app.get('/decode/:line', (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(`${req.params.line}`));
});

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});